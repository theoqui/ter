package parser;

import graphs.Graph;
import graphs.StopPoint;
import org.junit.Test;
import shortestPath.DijkstraPriorityQueue;
import shortestPath.Path;
import shortestPath.ShortestPathAlgorithm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CsvParserTest {

    @Test
    public void parseGtfs() {
        Graph network = CsvParser.parseGtfs("idfm_gtfs/stop_timesTest.csv", "idfm_gtfs/stopsTest.csv");
        System.out.println("Parsing complete, size of network : " + network.getSize());
        ShortestPathAlgorithm pathfinder = new DijkstraPriorityQueue();
        List<StopPoint> stopPoints = new ArrayList<>(network.getVertex());
        StopPoint start = network.get("StopPoint:A");
        StopPoint goal = network.get("StopPoint:F");
        Path shortestPath = pathfinder.getShortestPath(network, start, goal);

        assertTrue(shortestPath.getLength() - 15.0 <= 0.0000001);
    }
}