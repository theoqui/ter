package graphs;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GraphTest {
    private Graph graph;
    private StopPoint[] stops;


    @Before
    public void setup() {
        this.graph = new Graph();
        this.stops = new StopPoint[16];
        for (int i = 0; i < 16; i++) {
            stops[i] = new StopPoint(0, 0, ""+i, "");
        }
    }

    @Test
    public void addVertex() {
        assertEquals(0, graph.getSize());

        //  adding some vertex
        graph.addVertex(stops[1]);
        graph.addVertex(stops[4]);
        graph.addVertex(stops[5]);

        // the graph should now have size 3
        assertEquals(3, graph.getSize());

        // check that the vertex have actually been added
        assertTrue(graph.getVertex().contains(stops[1]));
        assertTrue(graph.getVertex().contains(stops[4]));
        assertTrue(graph.getVertex().contains(stops[5]));
    }

    @Test
    public void addEdge() {
        // -- by adding the vertex first
        graph.addVertex(stops[1]);
        graph.addVertex(stops[4]);

        graph.addEdge(stops[1], stops[4], 1.0);

        //  checking that the edges exist
        assertTrue(graph.getNeighbors(stops[1]).contains(stops[4]));

        // -- adding an edge between vertex not in the graph
        graph.addEdge(stops[2], stops[3], 9.0);

        // checking for the new edge
        assertTrue(graph.getNeighbors(stops[2]).contains(stops[3]));

        // checking that nothing weird is happening (like edges existing where they shouldn't)
        assertFalse(graph.getNeighbors(stops[1]).contains(stops[2]));
        assertFalse(graph.getNeighbors(stops[4]).contains(stops[3]));
    }

    @Test
    public void removeEdge() {
        graph.addEdge(stops[1], stops[2], 1.0);
        graph.addEdge(stops[3], stops[4], 2.0);
        graph.addEdge(stops[1], stops[4], 3.0);
        graph.addEdge(stops[2], stops[3], 2.0);

        graph.removeEdge(stops[3], stops[4]);

        assertFalse(graph.getNeighbors(stops[3]).contains(stops[4]));

        // trying to remove an edge that isn't there just to make sur it doesn't crash
        graph.removeEdge(stops[3], stops[4]);
    }

    @Test
    public void getWeight() {
        graph.addEdge(stops[1], stops[2], 1.0);
        graph.addEdge(stops[3], stops[4], 2.0);
        graph.addEdge(stops[1], stops[4], 3.0);
        graph.addEdge(stops[2], stops[3], 2.0);

        assertTrue(graph.getWeight(stops[1], stops[4]) - 3.0 < 0.00001);
    }
}