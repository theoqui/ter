package shortestPath;

import graphs.Graph;
import graphs.StopPoint;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class AStarTest {

    private StopPoint[] stops;
    private Random rand;

    @Before
    public void init() {
        this.rand = new Random();
        this.stops = new StopPoint[16];
        for (int i = 0; i < 16; i++) {
            stops[i] = new StopPoint(rand.nextDouble(), rand.nextDouble(), i+"", "");
        }
    }

    @Test
    public void getShortestPath() {
        Graph graph = new Graph();

        // adding the edges...
        graph.addEdge(stops[0], stops[1], 10.0);
        graph.addEdge(stops[0], stops[5], 7.0);
        graph.addEdge(stops[0], stops[13], 3.0);
        graph.addEdge(stops[1], stops[4], 3.0);
        graph.addEdge(stops[1], stops[2], 8.0);
        graph.addEdge(stops[7], stops[2], 1.0);
        graph.addEdge(stops[2], stops[8], 3.0);
        graph.addEdge(stops[2], stops[3], 7.0);
        graph.addEdge(stops[3], stops[8], 4.0);
        graph.addEdge(stops[3], stops[9], 2.0);
        graph.addEdge(stops[4], stops[7], 4.0);
        graph.addEdge(stops[4], stops[11], 8.0);
        graph.addEdge(stops[4], stops[14], 9.0);
        graph.addEdge(stops[4], stops[6], 2.0);
        graph.addEdge(stops[5], stops[4], 2.0);
        graph.addEdge(stops[5], stops[12], 2.0);
        graph.addEdge(stops[6], stops[13], 24.0);
        graph.addEdge(stops[7], stops[8], 9.0);
        graph.addEdge(stops[7], stops[11], 2.0);
        graph.addEdge(stops[8], stops[10], 7.0);
        graph.addEdge(stops[9], stops[10], 12.0);
        graph.addEdge(stops[11], stops[15], 4.0);
        graph.addEdge(stops[11], stops[14], 3.0);
        graph.addEdge(stops[12], stops[13], 6.0);
        graph.addEdge(stops[14], stops[15], 4.0);

        // expected shortest path
        List<StopPoint> shortestPath = Arrays.asList(stops[0], stops[5], stops[4], stops[7], stops[2], stops[8], stops[10]);
        Path expected = new Path(shortestPath, graph);

        ShortestPathAlgorithm aStar= new AStar();
        Path actual = aStar.getShortestPath(graph, stops[0], stops[10]);

        assertTrue(24 - expected.getLength() < 0.0001);
        double actualLength = actual.getLength();
        assertTrue(24 - actual.getLength() < 0.0001);
        assertEquals(expected, actual);
    }
}