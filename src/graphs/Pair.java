package graphs;

import java.util.Objects;

public class Pair<X, Y> {
    private X left;
    private Y right;

    public Pair(X left, Y right) {
        this.left = left;
        this.right = right;
    }

    public X getLeft() {
        return left;
    }

    public Y getRight() {
        return right;
    }

    public void setLeft(X left) {
        this.left = left;
    }

    public void setRight(Y right) {
        this.right = right;
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) {
            return false;
        } else {
            Pair obj = (Pair) o;
            if (obj.getRight() instanceof Float || obj.getRight() instanceof Double) {
                return obj.getLeft() == this.getLeft() && (Math.abs((double) obj.getRight() - (double) this.getRight()) < 0.00001);
            } else {
                return obj.getLeft() == this.getLeft() && obj.getRight() == this.getRight();
            }
        }
    }
}