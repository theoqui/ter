package graphs;

import java.util.*;

public class Graph {
    private int size;   // = number of vertex
    private Map<StopPoint, Map<StopPoint, Double>> adjacency;  // To each node we associate a list of (vertex, weight)
    private Map<String, StopPoint> nameVertex;
    private List<Pair<StopPoint, StopPoint>> edges;
    public Graph() {
        this.size = 0;
        this.adjacency = new HashMap<>();
        this.edges = new LinkedList<>();
    }

    public void addVertex(StopPoint v) {
        if (!this.adjacency.containsKey(v)) {   // only add the vertex if it isn't in the graph
            this.adjacency.put(v, new HashMap<>());
            this.size++;
        }
    }

    public void addEdge(StopPoint v1, StopPoint v2, Double weight) {
        this.addVertex(v1); // adds the vertex if they aren't already in the graph
        this.addVertex(v2);

        this.edges.add(new Pair<>(v1, v2));
        this.adjacency.get(v1).put(v2, weight);
    }

    public Set<StopPoint> getVertex() {
        return this.adjacency.keySet();
    }

    public StopPoint get(String stopName) {
        return this.nameVertex.get(stopName);
    }

    public Set<StopPoint> getNeighbors(StopPoint v) {
        // v MUST be in the graph
        return this.adjacency.get(v).keySet();
    }

    public void removeEdge(StopPoint v1, StopPoint v2) {
        this.adjacency.get(v1).remove(v2);
    }

    public Double getWeight(StopPoint v1, StopPoint v2) {
        return this.adjacency.get(v1).get(v2);
    }

    public boolean contains(StopPoint v) {
        return this.adjacency.keySet().contains(v);
    }

    public int getSize() {
        return size;
    }

    public void setNameVertex(Map<String, StopPoint> m) {
        this.nameVertex = m;
    }

    public List<Pair<StopPoint, StopPoint>> getEdges() {
        return edges;
    }
}
