package graphs;

public class StopPoint implements Comparable<StopPoint> {
    private double latitude;
    private double longitude;
    private double priority;
    private String name;
    private String stopArea;

    public StopPoint(double latitude, double longitude, double priority, String name, String stopArea) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.priority = priority;
        this.name = name;
        this.stopArea = stopArea;
    }

    public StopPoint(double latitude, double longitude, String name, String stopArea) {
        this(latitude, longitude, Double.MAX_VALUE, name, stopArea);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public String getStopArea() {
        return stopArea;
    }

    @Override
    public int compareTo(StopPoint stopPoint) {
        if (stopPoint.priority < this.priority) {
            return 1;
        }
        if (stopPoint.priority > this.priority) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StopPoint) {
            return ((StopPoint) obj).name.equals(this.name);
        }
        return false;
    }
}
