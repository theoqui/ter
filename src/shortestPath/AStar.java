package shortestPath;

import graphs.Graph;
import graphs.StopPoint;
import parser.CsvParser;

import java.util.*;

public class AStar implements ShortestPathAlgorithm {

    public AStar() {

    }

    @Override
    public Path getShortestPath(Graph graph, StopPoint source, StopPoint dest) {
        Set<StopPoint> closedSet = new HashSet<>();
        Set<StopPoint> openSet = new HashSet<>();
        openSet.add(source);
        PriorityQueue<StopPoint> Q = new PriorityQueue<>();
        Map<StopPoint, Double> gScore = new HashMap<>();
        Map<StopPoint, Double> fScore = new HashMap<>();
        Map<StopPoint, StopPoint> prev = new HashMap<>();
        for (StopPoint p : graph.getVertex()) {
            gScore.put(p, Double.MAX_VALUE);
            fScore.put(p, Double.MAX_VALUE);
            prev.put(p, null);
        }
        gScore.put(source, 0.0);
        fScore.put(source, h(source, dest));
        Q.add(source);
        StopPoint current;
        double tentativeGScore;

        while (!Q.isEmpty()) {
            current = Q.remove();
            openSet.remove(current);

            if (current == dest) {
                break;
            }

            closedSet.add(current);
            for (StopPoint p : graph.getNeighbors(current)) {
                if (closedSet.contains(p)) {
                    continue;
                }

                tentativeGScore = addNoOverflow(gScore.get(current), graph.getWeight(current, p));

                if (!openSet.contains(p)) {
                    openSet.add(p);
                } else if (tentativeGScore >= gScore.get(p)) {
                    continue;
                }

                prev.put(p, current);
                gScore.put(p, tentativeGScore);
                fScore.put(p, tentativeGScore + h(p, dest));
                p.setPriority(fScore.get(p));
                Q.add(p);
            }
        }

        LinkedList<StopPoint> path = new LinkedList<>();
        StopPoint u = dest;
        if (Objects.nonNull(prev.get(u)) || u == source) {
            while (Objects.nonNull(u)) {
                path.addFirst(u);
                u = prev.get(u);
            }
        }

        return new Path(path, graph);
    }

    public Double h(StopPoint source, StopPoint dest) {
        return Math.sqrt(square(source.getLatitude() - dest.getLatitude()) + square(source.getLongitude() - dest.getLongitude()));
    }

    private double square(double d) {
        return d*d;
    }

    public static Double addNoOverflow(double a, double b) {
        if (Double.MAX_VALUE - a < b || Double.MAX_VALUE - b < a) {
            return Double.MAX_VALUE;
        } else {
            return a + b;
        }
    }
}
