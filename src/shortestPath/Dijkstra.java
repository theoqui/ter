package shortestPath;
import graphs.Graph;
import graphs.Pair;
import graphs.StopPoint;

import java.util.*;

public class Dijkstra implements ShortestPathAlgorithm {

    public Dijkstra() {
    }

    public Path getShortestPath(Graph graph, StopPoint source, StopPoint dest) {
        List<StopPoint> Q = new ArrayList<>();
        Map<StopPoint, Double> dist = new HashMap<>(); // distance from source
        Map<StopPoint, StopPoint> prev = new HashMap<>(); // previous vertex in the path

        for (StopPoint v : graph.getVertex()) {
            dist.put(v, Double.MAX_VALUE); // equivalent to infinity
            prev.put(v, null);  // undefined
        }
        dist.put(source, 0.0);    // the distance from the source to itself is 0

        Q.add(source);
        double alt;
        StopPoint u;
        long timeOut = System.currentTimeMillis() + 60000 * 10;
        while (!Q.isEmpty()) {
            if (System.currentTimeMillis() > timeOut) {
               return null;
            }

            u = this.getMinDist(dist, Q);
            Q.remove(u);
            for (StopPoint v : graph.getNeighbors(u)) {  // for all the neighbors of u
                if (System.currentTimeMillis() > timeOut) {
                    return null;
                }
                if (u == dest) {
                    break;
                }
                alt = dist.get(u) + graph.getWeight(u, v);   // adding the length of the edge between u and v
                if (alt < dist.get(v)) {
                    dist.put(v, alt);
                    prev.put(v, u);
                    Q.add(v);
                }
            }
        }

        LinkedList<StopPoint> path = new LinkedList<>();
        u = dest;
        if (Objects.nonNull(prev.get(u)) || u == source) {
            while (Objects.nonNull(u)) {
                path.addFirst(u);
                u = prev.get(u);
            }
        }

        return new Path(path, graph);
    }

    private StopPoint getMinDist(Map<StopPoint, Double> dist, List<StopPoint> vertices) {
        StopPoint minVertex = vertices.get(0);  // vertex with minimal distance, init to the first one
        double minDist = dist.get(minVertex);

        for (StopPoint v : vertices) {  // finding if there is a closer vertex
            if (dist.get(v) < minDist) {
                minDist = dist.get(v);
                minVertex = v;
            }
        }

        return minVertex;
    }
}
