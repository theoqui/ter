package shortestPath;
import graphs.Graph;
import graphs.Pair;
import graphs.StopPoint;

import java.util.*;

public class DijkstraPriorityQueue implements ShortestPathAlgorithm {

    public DijkstraPriorityQueue() {
    }

    @Override
    public Path getShortestPath(Graph graph, StopPoint source, StopPoint dest) {
        Map<StopPoint, Double> dist = new HashMap<>();   // distances from the source
        Map<StopPoint, StopPoint> prev = new HashMap<>(); // previous Node in the path
        Queue<StopPoint> Q = new PriorityQueue<>();

        dist.put(source, 0.0);

        for (StopPoint n : graph.getVertex()) {
            if (n != source) {
                dist.put(n, Double.MAX_VALUE);  // the minimal distance between two nodes is unknown for now
            }
            prev.put(n, null);  // we don't know the paths yet

            n.setPriority(dist.get(n)); // the priority queues orders using the cost -- which we'll use to represent the priority
        }

        Q.add(source);
        StopPoint u;
        double alt;
        long timeOut = System.currentTimeMillis() + 60000 * 10;
        while (!Q.isEmpty()) {
            if (System.currentTimeMillis() > timeOut) {
               return null;
            }
            u = Q.remove();
            //if (u == dest) { break; }
            for (StopPoint v : graph.getNeighbors(u)) {
                if (System.currentTimeMillis() > timeOut) {
                   return null;
                }

                if (u == dest) {
                    break;
                }
                alt = AStar.addNoOverflow(dist.get(u), graph.getWeight(u, v));
                if (alt < dist.get(v)) {
                    dist.put(v, alt);
                    prev.put(v, u);

                    v.setPriority(alt);
                    Q.add(v);
                }
            }
        }

        LinkedList<StopPoint> path = new LinkedList<>();
        u = dest;
        if (Objects.nonNull(prev.get(u)) || u == source) {
            while (Objects.nonNull(u)) {
                path.addFirst(u);
                u = prev.get(u);
            }
        }

        return new Path(path, graph);
    }
}
