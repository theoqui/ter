package shortestPath;

import graphs.Graph;
import graphs.StopPoint;

import java.util.List;

public class Path {
    private List<StopPoint> path;
    private double length;

    public Path(List<StopPoint> path, Graph graph) { // a path only exists in a given graph
        this.path = path;

        double length = 0;  // computing the length of the path in the graph
        for (int i = 0; i < path.size() - 1; i++) {
            length += graph.getWeight(path.get(i), path.get(i+1));
        }
        this.length = length;
    }

    public List<StopPoint> getPath() {
        return path;
    }

    public double getLength() {
        return this.length;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Path) {
            return ((Path) obj).getPath().equals(path) && (this.length - ((Path) obj).length < 0.0001);
        }
        return false;
    }
}
