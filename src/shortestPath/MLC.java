package shortestPath;

import graphs.Graph;
import graphs.Pair;
import graphs.StopPoint;

import java.util.*;

public class MLC implements ShortestPathAlgorithm {

    public MLC() {

    }

    @Override
    public Path getShortestPath(Graph graph, StopPoint source, StopPoint dest) {
        Map<StopPoint, Double> dist = new HashMap<>();
        Map<StopPoint, StopPoint> prev = new HashMap<>();
        for (StopPoint p : graph.getVertex()) {
            dist.put(p, Double.MAX_VALUE);
            prev.put(p, null);
        }

        dist.put(source, 0.0);


        Stream stream = new Stream();
        StopPoint u = source;
        double w;
        long timeOut = System.currentTimeMillis() + 60000 * 10;
        while (Objects.nonNull(u)) {
            if (System.currentTimeMillis() > timeOut) {
                return null;
            }
            for (StopPoint v : graph.getNeighbors(u)) {
                if (System.currentTimeMillis() > timeOut) {
                   return null;
                }
                w = graph.getWeight(u, v);
                if (dist.get(v) > dist.get(u) + w) {
                    dist.put(v, dist.get(u) + w);
                    prev.put(v, u);
                    stream.addNode(v);
                }
            }

            u = stream.selectNode();
        }

        LinkedList<StopPoint> path = new LinkedList<>();
        u = dest;
        if (Objects.nonNull(prev.get(u)) || u == source) {
            while (Objects.nonNull(u)) {
                path.addFirst(u);
                u = prev.get(u);
            }
        }

        return new Path(path, graph);
    }
}
