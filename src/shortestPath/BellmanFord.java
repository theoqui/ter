package shortestPath;

import graphs.Graph;
import graphs.Pair;
import graphs.StopPoint;

import java.util.*;

public class BellmanFord implements ShortestPathAlgorithm {

    public BellmanFord() {

    }

    @Override
    public Path getShortestPath(Graph graph, StopPoint source, StopPoint dest) {
        // returns null if the graph has a negative-weight cycle
        List<Pair<StopPoint, StopPoint>> edges = graph.getEdges();

        Map<StopPoint, Double> distance = new HashMap<>();
        Map<StopPoint, StopPoint> predecessor = new HashMap<>();

        for (StopPoint p : graph.getVertex()) {
            distance.put(p, Double.MAX_VALUE);
            predecessor.put(p, null);
        }

        distance.put(source, 0.0);

        StopPoint u;
        StopPoint v;
        double w;
        boolean keepGoing = true;
        long timeOut = System.currentTimeMillis() + 60000 * 10;
        for (int i = 0; i < graph.getSize(); i++) {
            if (!keepGoing) {
                break;
            }
            keepGoing = false;
            for (Pair<StopPoint, StopPoint> p : edges) {
                if (System.currentTimeMillis() > timeOut) {
                    return null;
                }
                u = p.getLeft();
                v = p.getRight();
                w = graph.getWeight(u, v);

                if (distance.get(u) + w < distance.get(v)) {    // if it's shorter to go through u, we go through u
                    keepGoing = true;
                    distance.put(v, distance.get(u)+w);
                    predecessor.put(v, u);
                }
            }
        }

        if (keepGoing) {
            for (Pair<StopPoint, StopPoint> p : edges) {
                u = p.getLeft();
                v = p.getRight();
                w = graph.getWeight(u, v);

                if (distance.get(u) + w < distance.get(v)) {
                    return null;    // negative-weight cycle
                }
            }
        }

        LinkedList<StopPoint> path = new LinkedList<>();
        u = dest;
        if (Objects.nonNull(predecessor.get(u)) || u == source) {
            while (Objects.nonNull(u)) {
                path.addFirst(u);
                u = predecessor.get(u);
            }
        }

        return new Path(path, graph);
    }
}
