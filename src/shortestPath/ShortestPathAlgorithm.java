package shortestPath;

import graphs.Graph;
import graphs.StopPoint;

public interface ShortestPathAlgorithm {
    public Path getShortestPath(Graph graph, StopPoint source, StopPoint dest);
}
