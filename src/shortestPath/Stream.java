package shortestPath;

import graphs.StopPoint;

import java.util.ArrayDeque;

public class Stream {
    private ArrayDeque<StopPoint> s1;
    private ArrayDeque<StopPoint> s2;

    public Stream() {
        this.s1 = new ArrayDeque<>();
        this.s2 = new ArrayDeque<>();
    }

    public void addNode(StopPoint p) {
        if (!this.s2.contains(p)) {
            s2.addLast(p);
        }
    }

    public StopPoint selectNode() {
        if (s1.isEmpty()) {
            if (s2.isEmpty()) {
                return null;
            }

            this.s1 = this.s2;
            this.s2 = new ArrayDeque<>();
        }
        return s1.pollFirst();
    }
}
