import com.google.common.base.Stopwatch;
import graphs.Graph;
import graphs.StopPoint;
import parser.CsvParser;
import parser.DimacsParser;
import shortestPath.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class Main {

    public static void main(String[] argc) {
        Stopwatch stopwatch = Stopwatch.createUnstarted();
        // Graph network = CsvParser.parseGtfs("idfm_gtfs/stop_times.csv", "idfm_gtfs/stops.csv");
        Graph network = DimacsParser.parse("dimacs/USA-road-d.NY.gr", "dimacs/USA-road-d.NY.co");
        System.out.println("Parsing complete, size of network : " + network.getSize());
        ShortestPathAlgorithm[] implementations = new ShortestPathAlgorithm[5];

        implementations[0] = new Dijkstra();
        implementations[1] = new DijkstraPriorityQueue();
        implementations[2] = new AStar();
        implementations[3] = new BellmanFord();
        implementations[4] = new MLC();

        StopPoint[] vertices = new StopPoint[network.getSize()];
        network.getVertex().toArray(vertices);

        Random random = new Random();
        /*String[] fromStops = new String[2];
        String[]toStops = new String[5];
        for (int i = 0; i < 5; i++) {
            if (i < 2) {
                fromStops[i] =
            }
        }*/

        /*// Paris
        String[] fromStops = new String[]{"StopPoint:59:4008888", "StopPoint:40:506", "StopPoint:59:4314756",
                "StopPoint:21:196", "StopPoint:58:146"};
        String[] toStops = new String[]{"StopPoint:59:3893399", "StopPoint:70:1025299436", "StopPoint:21:1009",
                "StopPoint:59:4015903", "StopPoint:13:54"};*/

        // NY
        String[] fromStops = new String[]{"131882", "185513", "69320", "771", "109879"};
        String[] toStops = new String[]{"121078", "205807"};

        /*// Florida
        String[] fromStops = new String[]{"924711", "457701", "987271", "106697", "765544"};
        String[] toStops = new String[]{"791290", "381743"};*/
        StopPoint[] sources = new StopPoint[5];
        StopPoint[] dest = new StopPoint[2];

        for (int i = 0; i < 5; i++) {  // selecting the sources and destinations
            if (i < 2) {
                dest[i] = network.get(toStops[i]);
            }
            sources[i] = network.get(fromStops[i]);
            /*if (i < 2) {
                dest[i] = vertices[random.nextInt(network.getSize())];
            }
            sources[i] = vertices[random.nextInt(network.getSize())];*/
            System.out.println("source " + i + " " + sources[i].getName() + "\t dest " + i + " " + dest[Math.min(i, 1)].getName());
        }

        Path path;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.println("From source " + i + " to destination " + j);
                for (int k = 0; k < 5; k++) {
                    if (k < 3) {
                        stopwatch.start();
                        path = implementations[k].getShortestPath(network, sources[i], dest[j]);
                        if (Objects.nonNull(path)) {
                            System.out.println("\t" + implementations[k].getClass().getSimpleName() +
                                    " : " + stopwatch.elapsed().getNano() + "ns (path length : " + path.getLength() + ")");
                        } else {
                            System.out.println("\t" + implementations[k].getClass().getSimpleName() + " : timeOut");
                        }
                        stopwatch.reset();
                    }
                }
            }
        }
    }
}
