package parser;

import graphs.Graph;
import graphs.StopPoint;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public abstract class CsvParser {

    public static Graph parseGtfs(String stopTimes, String stops) {
        try (BufferedReader stopTimesReader = new BufferedReader(new FileReader(stopTimes));
             BufferedReader stopsReader = new BufferedReader(new FileReader(stops))) {

            stopsReader.readLine(); // consuming the headers

            Graph network = new Graph();
            Map<String, StopPoint> stopPoints = new HashMap<>();    // to find the stopPoints by name
            Map<String, List<StopPoint>> stopAreas = new HashMap<>();

            String line;
            String[] stop;
            double latitude;
            double longitude;
            StopPoint newStop;

            while((line = stopsReader.readLine()) != null) {
                stop = line.split(",");
                if (stop[0].split(",")[0].equals("StopArea")) {
                    stopAreas.put(stop[0], new LinkedList<>());
                }

                if (stop[0].split(":")[0].equals("StopPoint")) {    // if we're looking at a stop point
                    try {
                        latitude = Double.parseDouble(stop[3]); // trying to parse the coordinates as doubles
                        longitude = Double.parseDouble(stop[4]);
                        newStop = new StopPoint(latitude, longitude, stop[0], stop[8]);
                        stopPoints.put(stop[0], newStop);
                        network.addVertex(newStop);
                        if (Objects.isNull(stopAreas.get(stop[8]))) {
                            stopAreas.put(stop[8], new LinkedList<>());
                        }
                        stopAreas.get(stop[8]).add(newStop);
                    } catch (NumberFormatException e) {
                        System.err.println(line);
                    }
                }
            }

            StopPoint lastStopPoint;
            StopPoint current;

            stop = stopTimesReader.readLine().split(",");   // needed to initialize lastStopPoint (this should be the header
            lastStopPoint = stopPoints.get(stop[3]);

            while((line = stopTimesReader.readLine()) != null) {
                stop = line.split(",");
                if (!stop[4].equals("0")) { // if we are not at the beginning of a trip, we add an edge with the previous StopPoint
                    current = stopPoints.get(stop[3]);
                    if (!Objects.isNull(current) && !Objects.isNull(lastStopPoint)) {
                        network.addEdge(lastStopPoint, current, distance(lastStopPoint, current));
                    }
                }

                lastStopPoint = stopPoints.get(stop[3]);
            }

            for (List<StopPoint> stopsInArea : stopAreas.values()) {
                for (StopPoint p1 : stopsInArea) {
                    for (StopPoint p2 : stopsInArea) {
                        if (!p1.equals(p2)) {
                            network.addEdge(p1, p2, distance(p1, p2));
                        }
                    }
                }
            }

            network.setNameVertex(stopPoints);
            return network;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static double distance(StopPoint p1, StopPoint p2) {
        return Math.abs(p1.getLatitude() - p2.getLatitude()) + Math.abs(p1.getLongitude() - p2.getLongitude());
    }
}
