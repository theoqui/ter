package parser;

import graphs.Graph;
import graphs.StopPoint;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class DimacsParser {

    public static Graph parse(String graph, String coordinates) {
        Graph network = new Graph();
        Map<String, StopPoint> nodes = new HashMap<>();
        try (BufferedReader graphReader = new BufferedReader(new FileReader(graph));
             BufferedReader coordReader = new BufferedReader(new FileReader(coordinates))) {

            Map<String, StopPoint> stopPoints = new HashMap<>();
            String line;
            String[] stop;
            double latitude;
            double longitude;
            StopPoint newStop;

            while((line = coordReader.readLine()) != null) {
                stop = line.split(" ");
                if (stop[0].equals("v")) {
                    latitude = Double.parseDouble(stop[2]);
                    longitude = Double.parseDouble(stop[3]);
                    newStop = new StopPoint(latitude, longitude, Double.MAX_VALUE, stop[1], " ");
                    stopPoints.put(newStop.getName(), newStop);
                    nodes.put(stop[1], newStop);
                    network.addVertex(newStop);
                }
            }

            while((line = graphReader.readLine()) != null) {
                stop = line.split(" ");
                if (stop[0].equals("a")) {
                    network.addEdge(nodes.get(stop[1]), nodes.get(stop[2]), Double.parseDouble(stop[3]));
                }
            }
            network.setNameVertex(stopPoints);
            return network;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return network;
    }
}
